package com.company.homework5;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;

public class SingleList <T> implements List<T> {
    private int length;
    private Node<T> head;
    private Node<T> tail;

    @Data
    @Accessors(chain = true)
    private static class Node<T> {
        private T value;
        private Node next;
    }

    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for (Node<T> cur = head; cur != null; cur = cur.getNext()) {
            if (Objects.equals(cur.getValue(), o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> cur = head;

            @Override
            public boolean hasNext() {
                return cur != null;
            }

            @Override
            public T next() {
                T value = cur.getValue();
                cur = cur.getNext();
                return value;
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        a = Arrays.copyOf(a, length);
        int i = 0;
        for (T t : this) {
            a[i++] = (T1) t;
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        if (head == null) {
            head = tail = new Node<T>().setValue(t);
        } else {
            Node<T> node = new Node<T>().setValue(t);
            tail.setNext(node);
            tail = node;
        }
        length++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if(Objects.equals(head.getValue(), o)){
            head=head.next;
            length--;
            return true;
        }
        for (Node<T> cur = head; cur != null; cur = cur.next) {
            if (Objects.equals(cur.next.getValue(), o)) {
                cur.next=cur.next.next;
                length--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (var t : c) {
            if (!contains(t)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(this::add);
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;

        for (int i = 0; i < index; i++) {
            cur = cur.getNext();
        }

        for (T t : c) {
            Node<T> node = new Node<>();
            node.setValue(t);
            node.next=cur.next;
            cur.next=node;
            cur=cur.next;
            length++;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        c.forEach(this::remove);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Node<T> cur = head; cur != null; cur = cur.getNext()) {
            int trueCounter=0;
            for (var t : c) {
                if (Objects.equals(cur.getValue(), t)) trueCounter++;
            }
            if(trueCounter==0)remove(cur.value);
        }
        return true;
    }

    @Override
    public void clear() {
        head = tail = null;
        length = 0;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();

        Node<T> cur = head;

        for (int i = 0; i < index; i++) {
            cur = cur.getNext();
        }

        return cur.getValue();
    }

    @Override
    public T set(int index, T element) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();

        Node<T> cur = head;

        for (int i = 0; i < index; i++) {
            cur = cur.getNext();
        }

        cur.setValue(element);

        return element;
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;

        for (int i = 0; i < index; i++) {
            cur = cur.getNext();
        }

        Node<T> node = new Node<>();
        node.setValue(element);
        node.next=cur.next;
        cur.next=node;
        cur=cur.next;
        length++;
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= length) throw new IndexOutOfBoundsException();
        Node<T> cur = head;

        for (int i = 0; i < index-1; i++) {
            cur = cur.getNext();
        }

        T value = cur.getValue();
        cur.next=cur.next.next;
        length--;

        return value;
    }

    @Override
    public int indexOf(Object o) {
        int i = 0;
        for (Node<T> cur = head; cur != null; cur = cur.getNext(),i++) {
            if (Objects.equals(cur.getValue(), o)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        int i = 0;
        int index=-1;
        for (Node<T> cur = head; cur != null; cur = cur.getNext(),i++) {
            if (Objects.equals(cur.getValue(), o)) {
                index=i;
            }
        }
        return index;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (fromIndex < 0 || fromIndex >= length) throw new IndexOutOfBoundsException();
        if (toIndex < 0 || toIndex >= length) throw new IndexOutOfBoundsException();
        List<T> list = new BIList<>();

        Node<T> cur = head;

        for (int i = 0; i < fromIndex; i++) {
            cur = cur.getNext();
        }

        for (int i = fromIndex; i <= toIndex; i++) {
            list.add(cur.value);
            cur=cur.next;
        }
        return list;
    }
}
