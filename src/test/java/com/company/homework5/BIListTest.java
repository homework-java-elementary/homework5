package com.company.homework5;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BIListTest {
    @Test
    @DisplayName("Добавление и получение")
    public void testAddAndGet() {
        List<Integer> list = new BIList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(5);
        list.add(2, 4);

        assertEquals(5, list.size());
        assertEquals(1, list.get(0));
        assertEquals(2, list.get(1));
        assertEquals(3, list.get(2));
        assertEquals(4, list.get(3));
        assertEquals(5, list.get(4));


        List<Integer> list1 = new BIList<>();
        list1.add(10);
        list1.add(100);
        list.addAll(1, list1);

        assertEquals(7, list.size());
        assertArrayEquals(new Integer[]{1, 2, 10, 100, 3, 4, 5}, list.toArray(new Integer[0]));


        list.addAll(list1);

        assertArrayEquals(new Object[]{1, 2, 10, 100, 3, 4, 5, 10, 100}, list.toArray());

    }

    @Test
    @DisplayName("Удаление и изменение")
    public void testDeleteAndSet() {
        List<Integer> list = new BIList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        list.set(2, 10);

        assertEquals(5, list.size());
        assertArrayEquals(new Integer[]{1, 2, 10, 4, 5}, list.toArray(new Integer[0]));
        assertFalse(list.isEmpty());

        list.remove(1);

        assertEquals(4, list.size());
        assertArrayEquals(new Integer[]{1, 10, 4, 5}, list.toArray(new Integer[0]));

        list.remove((Object) 1);

        assertEquals(3, list.size());
        assertArrayEquals(new Integer[]{10, 4, 5}, list.toArray(new Integer[0]));

        List<Integer> list1 = new BIList<>();
        list1.add(10);
        list1.add(5);
        list.removeAll(list1);

        assertEquals(1, list.size());
        assertArrayEquals(new Integer[]{4}, list.toArray(new Integer[0]));

        list.clear();

        assertTrue(list.isEmpty());

        list.add(1);
        list.add(4);
        list.add(10);
        list.add(5);
        list.add(7);

        list.retainAll(list1);
        assertArrayEquals(new Integer[]{10, 5}, list.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Сравнение")
    public void testContains() {
        List<Integer> list = new BIList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        List<Integer> list1 = new BIList<>();
        list1.add(1);
        list1.add(5);

        assertTrue(list.contains((Object) 1));
        assertTrue(list.contains((Object) 3));
        assertTrue(list.contains((Object) 5));
        assertFalse(list.contains((Object) 10));

        assertTrue(list.containsAll(list1));
        list1.add(10);
        assertFalse(list.containsAll(list1));
    }

    @Test
    @DisplayName("Поиск по значению")
    public void testIndexsOf() {
        List<Integer> list = new BIList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(10);
        list.add(1);
        list.add(4);
        list.add(5);

        assertEquals(0, list.indexOf(1));
        assertEquals(3, list.indexOf(4));

        assertEquals(5, list.lastIndexOf(1));
        assertEquals(6, list.lastIndexOf(4));
    }

    @Test
    @DisplayName("Список по интервалу")
    public void testSubList() {
        List<Integer> list = new BIList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        assertArrayEquals(new Integer[]{2, 3, 4}, list.subList(1, 3).toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Реверсеный итератор")
    public void testReverseIterator() {
        BIList<String> list = new BIList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");
        Iterator<String> it = list.reverseIterator();

        String result = "";
        while(it.hasNext()){
            result+=it.next();
        }
        assertEquals("edcba", result);
    }

    @Test
    @DisplayName("Итератор")
    public void testIterator() {
        List<String> list = new BIList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");

        String result = "";
        for(String s:list){
            result+=s;
        }
        assertEquals("abcde", result);
    }
}